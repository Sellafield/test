﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Geometry;
using Geometry.Models;

namespace GeometryTests
{
    [TestClass]
    public class GeometryServiceTests
    {
        [TestMethod]
        public void Calculate_Square_Successfull()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            DecimalOperation result = geometryService.TriangleSquare(3, 4, 5);
            //Assert
            Assert.IsTrue(result.Success);
        }
        
        [TestMethod]
        public void Calculate_Square_Value()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            DecimalOperation result = geometryService.TriangleSquare(3, 4, 5);
            //Assert
            Assert.AreEqual((decimal)3.5, result.Value);
        }

        [TestMethod]
        public void Calculate_Square_Value_Other_Order()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            DecimalOperation result = geometryService.TriangleSquare(5, 3, 4);
            //Assert
            Assert.AreEqual((decimal)3.5, result.Value);
        }

        [TestMethod]
        public void Calculate_Square_Failed_All_Zeros_Or_Below()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            DecimalOperation result = geometryService.TriangleSquare(0, -1, -2);
            //Assert
            Assert.IsFalse(result.Success);
            Assert.AreEqual("Одна или несколько сторон имеют неверную длину", result.ErrorMessage);
        }

        [TestMethod]
        public void Calculate_Square_Failed_Triangle_Not_Square()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            DecimalOperation result = geometryService.TriangleSquare(1, 4, 57);
            //Assert
            Assert.IsFalse(result.Success);
            Assert.AreEqual("Треугольник не прямоугольный", result.ErrorMessage);
        }

        [TestMethod]
        public void Hyper_Add()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            double result = geometryService.Hyper(2,1,3);
            //Assert            
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void Hyper_Sub()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            double result = geometryService.Hyper(8, -1, 5);
            //Assert            
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void Hyper_Mult()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            double result = geometryService.Hyper(5, 2, 4);
            //Assert            
            Assert.AreEqual(20, result);
        }

        [TestMethod]
        public void Hyper_Div()
        {
            //Arrange
            var geometryService = new GeometryService();
            //Act
            double result = geometryService.Hyper(9, -2, 2);
            //Assert            
            Assert.AreEqual(4.5, result);
        }
    }
}
