﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Models
{
    public class DecimalOperation : Operation
    {
        public decimal Value { get; private set; }

        public DecimalOperation(decimal value) : base()
        {
            Value = value;
        }

        public DecimalOperation(string errorMessage) : base(errorMessage)
        {

        }

    }
}
