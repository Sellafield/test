﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Models
{
    public class Operation
    {
        public bool Success { get; private set; }
        public string ErrorMessage { get; private set; }

        public Operation()
        {
            Success = true;
        }

        public Operation(string errorMessage)
        {
            Success = false;
            ErrorMessage = errorMessage;
        }
    }
}
