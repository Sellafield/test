﻿using Geometry.Models;
using System;
using System.Linq;

namespace Geometry
{
    public class GeometryService
    {
        public DecimalOperation TriangleSquare(decimal a, decimal b, decimal c)
        {
            decimal result = 0;

            if (a <= 0 || b <= 0 || c <= 0)
                return new DecimalOperation("Одна или несколько сторон имеют неверную длину");

            //т.к. заранее неизвестно, какие стороны являются катетами, определим их, например, математически
            decimal[] TriangleSides = new decimal[] { a, b, c };

            var gipotenuse = TriangleSides.Max();

            var catetA = TriangleSides.Min();

            var catetB = TriangleSides.Sum() - gipotenuse - catetA;

            var isSquare = (gipotenuse * gipotenuse) == (catetA * catetA) + (catetB * catetB);

            if (!isSquare)
                return new DecimalOperation("Треугольник не прямоугольный");

            try
            {                                
                result = (catetA + catetB) / 2;
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("В ходе вычисления произошла ошибка:/n{0}", ex.Message);
                return new DecimalOperation(errorMessage);
            }

            return new DecimalOperation(result);
        }

        /// <summary>
        /// По мотивам гипероператоров. Метод позволяет свести вычисление и хранение
        /// математических операций к виду (X, opCode, Y), где X и Y - операнды, а
        /// opCode - код математической операции, такой, что:
        /// 1 - сложение
        /// -1 - вычитание
        /// 2 - умножение
        /// -2 - деление
        /// Прочие операции гипероператоров в данном примере не рассматриваются.
        /// </summary>
        /// <param name="X">Первый операнд</param>
        /// <param name="opCode">Код операции</param>
        /// <param name="Y">Второй операнд</param>
        /// <returns>Результат вычисления</returns>
        public double Hyper(double X, int opCode, double Y)
        {
            if (Math.Sqrt(opCode * opCode) % 2 > 0)
                return X + (opCode * Y);
            else
                return X * (Math.Pow(Y, opCode / 2));
        }
    }
}
